@base <http://anabasis-assets.com/ontologies/boulangerie/Four>
$rdfs:label("Fours"@fr)
"Fours et autres appareils de cuissons"@fr

class ModeleFour {
  $rdfs:label("Modèle de four"@fr)
  "Caractéristique du four tel que vendu par le fabriquant."@fr

  @require(rdfs:label)

  has temperatureMax: xsd:float {
    $rdfs:label("Température maximale (°C)"@fr)
    "Température maximale à laquel le four peut chauffer."@fr
  }

  has puissanceChauffekW: xsd:float {
    $rdfs:label("Puissance de chauffe (kW)"@fr)
    "Puissance consommée par le four pendant la chauffe."@fr
  }
}

class ModeleFourPain subClassOf ModeleFour {
  $rdfs:label("Modèle de four à pain"@fr)
  "Caractéristique du four à pain tel que vendu par le fabriquant."@fr

  has capaciteBaguetteMin: xsd:int {
    $rdfs:label("Capacité baguette minimale"@fr)
    "Nombre minimal de baguettes de 250g pour une fournée."@fr
  }

  has capaciteBaguetteMax: xsd:int {
    $rdfs:label("Capacité baguette maximale"@fr)
    "Nombre maximal de baguettes de 250g pour une fournée."@fr
  }

  has capacitePainMin: xsd:int {
    $rdfs:label("Capacité pain minimale"@fr)
    "Nombre minimal de pains de 400g pour une fournée."@fr
  }

  has capacitePainMax: xsd:int {
    $rdfs:label("Capacité pain maximale"@fr)
    "Nombre maximal de pains de 400g pour une fournée."@fr
  }
}

rule modele_peut_cuire_pain {
  "Modèle de four peut cuire pain"@fr
  if
    ?modele a ModeleFour and
    ?modele.temperatureMax > 270
  then
    ?modele a ModeleFourPain
}

class Four {
  $rdfs:label("Four"@fr)
  "Four physiquement présent dans la boulangerie."@fr

  @require(rdfs:label)

  has modele: ModeleFour {
    $rdfs:label("modèle du four"@fr)
    "Lien entre un modèle de four et les fours de ce modèle"@fr
  }
}

class FourPain subClassOf Four {
  $rdfs:label("Four à pain"@fr)

  "Four à pain physiquement présent dans la boulangerie."@fr
}

rule four_est_a_pain {
  "Four est four à pain"@fr
  if
    ?four modele ?modele and
    ?modele a ModeleFourPain
  then
    ?four a FourPain
}

query caracteristiques_modele_four(?modele, ?t_max, ?pkw) {
  $rdfs:label("Caractéristiques des modèles de four"@fr)
  "Liste les modèles de fours et leurs caractéristiques"@fr

  ?modele a ModeleFour and
  ?modele temperatureMax ?t_max and
  ?modele puissanceChauffekW ?pkw
}

query caracteristiques_modele_four_pain(?modele, ?t_max, ?b_max, ?b_min, ?p_max, ?p_min, ?pkw) {
  $rdfs:label("Caractéristiques des modèles de four à pain"@fr)
  "Liste les modèles de fours à pain et leurs caractéristiques"@fr

  ?modele a ModeleFourPain and
  caracteristiques_modele_four(?modele, ?t_max, ?pkw) and
  ?modele capaciteBaguetteMin ?b_min and
  ?modele capaciteBaguetteMax ?b_max and
  ?modele capacitePainMin ?p_min and
  ?modele capacitePainMax ?p_max
}


query fours_boulangerie(?boulangerie, ?four, ?modele) {
    $rdfs:label("Fours par boulangerie"@fr)
    "Liste l'ensemble des fours et leurs modèles, par boulangerie."

    ?boulangerie bo:cuitAvec ?four and
    ?four modele ?modele and
    ?boulangerie a bo:Boulangerie and
    ?four a Four and
    ?modele a ModeleFour
}

query fours_pain(?boulangerie, ?four, ?modele) {
    $rdfs:label("Fours à pain par boulangerie"@fr)
    "Liste l'ensemble des fours à pain et leurs modèles, par boulangerie."

    fours_boulangerie(?boulangerie, ?four, ?modele) and
    ?modele a ModeleFourPain
}
